#version 120

attribute vec3 position;
attribute vec3 normal;

uniform mat4 modelView;
uniform mat4 projection;
uniform mat4 shadowMat;

varying vec4 norm;
varying vec4 shadow;

void main() {
	shadow = shadowMat * vec4(position, 1.0);
	gl_Position = projection * modelView * vec4(position, 1.0);
	norm = modelView * vec4(normal, 0.0);
}