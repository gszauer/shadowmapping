#version 120

uniform vec4 lightDirection;
uniform sampler2D shadowMap;

varying vec4 norm;
varying vec4 shadow;

void main() {	
	vec4 shadowCoordinateWdivide = shadow / shadow.w;
	// Used to lower moiré pattern and self-shadowing
	shadowCoordinateWdivide.z += 0.0005;

	float distanceFromLight = texture2D(shadowMap,shadowCoordinateWdivide.st).z;
	float shadowIntensity = 1.0;
	if (shadow.w > 0.0)
	 	shadowIntensity = distanceFromLight < shadowCoordinateWdivide.z ? 0.25 : 1.0 ;

	float diffuseIntensity = max(0.0, dot(normalize(norm), -lightDirection));
    
	//gl_FragColor = shadowIntensity;
	//gl_FragColor = vec4(diffuseIntensity)
	gl_FragColor = vec4(diffuseIntensity) * shadowIntensity;
}