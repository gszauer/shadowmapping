#ifndef _H_SHADER_
#define _H_SHADER_

#if __linux
    #include <glm/glm.hpp>
    #include <glm/ext.hpp>
#else
    #include "glm/glm.hpp"
    #include "glm/ext.hpp"
#endif
#include <string>
#include <vector>
#include <map>

class Shader {
protected:
    unsigned int                        m_nShaderId;
    std::vector<unsigned int>           m_vUniformLocations;
    std::vector<std::string>            m_vUniformNames;
    std::vector<unsigned int>           m_vUniformTypes;
    std::map<std::string, unsigned int> m_mapUniformLocations;
    bool                                m_bError;
public:
    static const int kUniformInt        = 0;
    static const int kUniformFloat      = 2;
    static const int kUniformVector     = 4;
    static const int kUniformMatrix     = 8;
    static const int kUniformBool       = 16;
    static const int kUniformSampler    = 64;
    static const int kUniformUnsupported= 32;
protected:
    Shader(const Shader&);
    Shader& operator=(const Shader&);
    void PopulateUniforms();
    std::string ReadFile(const char* path);
    unsigned int CompileShader(const char* shader, unsigned int type);
    unsigned int MakeProgram(unsigned int vertex, unsigned int fragment);
public:
    Shader(const char* shader);
    ~Shader();
    
    void Bind();
    void Unbind();
    
    unsigned int GetHandle();
    int GetUniformCount();
    std::string GetUniformName(int index);
    int GetUniformIndex(const std::string& name);
    unsigned int GetUniformType(int index);
    bool ContainsUniform(const std::string& name);
    unsigned int GetUniformLocation(int index);
    unsigned int GetUniformLocation(const std::string& name);
    int GetNumUniforms();
    
    void SetInt(int uniform, int value);
    void SetFloat(int uniform, float value);
    void SetVector(int uniform, const glm::vec4& value);
    void SetMatrix(int uniform, const glm::mat4& value);
    void SetBool(int uniform, bool value);
    
    void SetInt(const std::string& uniform, int value);
    void SetFloat(const std::string& uniform, float value);
    void SetVector(const std::string& uniform, const glm::vec4& value);
    void SetMatrix(const std::string& uniform, const glm::mat4& value);
    void SetBool(const std::string& uniform, bool value);
    
    int GetAttribLocation(const std::string& attribName);
    bool GetError();
};

#endif