#include "ObjLoader.h"
#include "Shader.h"
#include "DepthDebug.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <iostream>
#if __APPLE__
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#elif __linux
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#elif _WIN32
    #include <Windows.h>
    #include <gl/GL.h>
    #include <gl/GLU.h>
    #include "GL/openglut.h"
#else
    #error Include OpenGL Headers
#endif
#if __linux
    #include <glm/glm.hpp>
    #include <glm/ext.hpp>
#else
    #include "glm/glm.hpp"
    #include "glm/ext.hpp"
#endif
#if __linux | __APPLE__
    #include <sys/time.h>
#endif

// Glew: http://lazyfoo.net/tutorials/OpenGL/15_extensions_and_glew/index.php
// VBO info: http://en.wikipedia.org/wiki/Vertex_Buffer_Object
// More VBO: http://www.opengl.org/wiki/VBO_-_just_examples
// Following: http://fabiensanglard.net/shadowmapping/index.php

#define WINFOW_TITLE    "Shadow Mapping"
#define WINDOW_WIDTH    800
#define WINDOW_HEIGHT   600
#define UPDATE_TIME     17 /* ((1 / 60) * 1000) rounded up */

GLuint fboId = 0;
GLuint depthTextureId = 0;

Obj* cube = 0;
Obj* sphere = 0;
Obj* monkey = 0;
Obj* plane = 0;

Shader* diffuse = 0;
Shader* shadow = 0;

glm::mat4 modelView;
glm::mat4 projection;
glm::vec2 cameraAngle = glm::vec2(60.0f, 35.0f);

int g_nWidth;
int g_nHeight;
double lastTime = 0.0f;

void Initialize();
void Shutdown();

void display();
void updateTimer(int windowId);
void reshape(int width, int height);

glm::mat4 RenderShadowMap(glm::vec3 lightPosition);
void RenderScene(glm::mat4 shadowMat, glm::vec3 lightPosition);
glm::vec3 GetLightPosition(float distance);
void GenerateShadowTexture();
void GenerateShadowFBO();
glm::mat4 GetShadowMatrix();
void RenderLight(glm::vec3 normPosition);
double GetMilliseconds();

int main(int argc, char** argv) {
    glutInit(&argc, argv);
        
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(WINDOW_WIDTH , WINDOW_HEIGHT);
    glutCreateWindow(WINFOW_TITLE);

#if !__APPLE__
    GLenum glewError = glewInit(); 
    if(glewError != GLEW_OK) { 
        std::cout << "Error! Could not load OpenGL extensions!\n";
        std::cout << "\t" << glewGetErrorString(glewError) << "\n";
        return 0;
    }
#endif
    
    Initialize();
    reshape(WINDOW_WIDTH, WINDOW_HEIGHT);
    atexit(Shutdown);
    
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIgnoreKeyRepeat(false); // Key is continually held down
    glutTimerFunc(UPDATE_TIME, updateTimer, glutGetWindow());
    
    glutMainLoop();
    
    return 0;
}

void Initialize() {
    glLoadIdentity();
    
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH); 
    
    GenerateShadowTexture();
    GenerateShadowFBO();
    
    cube = new Obj("Objects/Cube.obj");
    sphere = new Obj("Objects/Sphere.obj");
    monkey = new Obj("Objects/Monkey.obj");
    plane = new Obj("Objects/Plane.obj");
    
    diffuse = new Shader("Shaders/diffuse");
    shadow = new Shader("Shaders/shadow");
    if (diffuse->GetError() || shadow->GetError()) {
        exit(0);
    }
    
    lastTime = GetMilliseconds();
}

void Shutdown() {
    delete cube;
    delete sphere;
    delete monkey;
    delete plane;
    delete diffuse;
    delete shadow;
    
    if (depthTextureId != 0)
        glDeleteTextures(1, &depthTextureId);
    if (fboId != 0)
        glDeleteFramebuffersEXT(1, &fboId);
}

void GenerateShadowTexture() {
    int shadowMapWidth = 1024;
    int shadowMapHeight = 1024;
    
    // Create depth texture component
    glGenTextures(1, &depthTextureId);
    glBindTexture(GL_TEXTURE_2D, depthTextureId);
    
    // GL_LINEAR does not make sense for a depth texture
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    // Remove artifacts on edges of the shadow map
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
    
     // No need to force GL_DEPTH_COMPONENT24, drivers usually give you the max precision if available
    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapWidth, shadowMapHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void GenerateShadowFBO() {
    GLenum fboStatus;
    
    // Create the frame buffer object
    glGenFramebuffersEXT(1, &fboId);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboId);
    
    // Instruct openGL that we won't bind a color texture with the currently bound FBO
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    // attach the texture to FBO depth attachment point
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,GL_TEXTURE_2D, depthTextureId, 0);

    // check FBO status
    fboStatus = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    if(fboStatus != GL_FRAMEBUFFER_COMPLETE_EXT)
        std::cout << "GL_FRAMEBUFFER_COMPLETE_EXT failed, CANNOT use FBO\n";

    // switch back to window-system-provided framebuffer
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

glm::mat4 GetShadowMatrix() {
    // Move from unit cube [-1, 1] to [0, 1[
    glm::mat4 bias = glm::mat4 (
        0.5f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.5f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.5f, 0.0f,
        0.5f, 0.5f, 0.5f, 1.0f
    );
    
    return bias * projection * modelView;
}

glm::vec3 GetLightPosition(float distance) {
    static const float PI_180 = (M_PI/180.0f);
    float camX = distance * -sinf(cameraAngle.x*PI_180) * cosf((cameraAngle.y)*PI_180);
    float camY = distance * -sinf((cameraAngle.y)*PI_180);
    float camZ = -distance * cosf((cameraAngle.x)*PI_180) * cosf((cameraAngle.y)*PI_180);
    return glm::vec3(camX, camY, camZ);
}

glm::mat4 RenderShadowMap(glm::vec3 lightPosition) {
    // Render shadow map
    glBindFramebuffer(GL_FRAMEBUFFER, fboId);
    glViewport(0, 0, 1024, 1024);
    glClearDepth(1.0f);
    glClear(GL_DEPTH_BUFFER_BIT);
    glCullFace(GL_FRONT);
    glClear( GL_DEPTH_BUFFER_BIT); // Gotta clear that depth buffer!
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);  // Disable color mask, so we only write to depth
    
    glm::mat4 shadowMat = GetShadowMatrix();
    
    shadow->Bind();
    shadow->SetMatrix("modelView", modelView);
    shadow->SetMatrix("projection", projection);
    static int shadowPosition = shadow->GetAttribLocation("position");
    monkey->Render(shadowPosition);
    plane->Render(shadowPosition);
    shadow->Unbind();
            
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); 
    glCullFace(GL_BACK);
    
    return shadowMat;
}

void RenderScene(glm::mat4 shadowMat, glm::vec3 lightPosition) {
    glClearColor(0.5f, 0.6f, 0.7f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    diffuse->Bind();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthTextureId);
    diffuse->SetInt("shadowMap", 0);
    
    diffuse->SetMatrix("modelView", modelView);
    diffuse->SetMatrix("projection", projection);
    diffuse->SetMatrix("shadowMat", shadowMat);
    diffuse->SetVector("lightDirection", glm::vec4(lightPosition, 1.0));
    
    static int positionAttrib = diffuse->GetAttribLocation("position");
    static int normalAttrib = diffuse->GetAttribLocation("normal");
    
    monkey->Render(positionAttrib, normalAttrib);
    plane->Render(positionAttrib, normalAttrib);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    diffuse->Unbind();
}

void display() {
    glm::vec3 lightPosition = glm::normalize(GetLightPosition(3.0f));
    
    modelView = glm::lookAt(-lightPosition, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    projection = glm::ortho(-7.0f, 7.0f, -7.0f, 7.0f, -7.5f, 7.5f) ; // Arbitrary, contains everything!
    glm::mat4 shadowMat = RenderShadowMap(lightPosition);
    
    reshape(g_nWidth, g_nHeight);
    modelView = glm::lookAt(glm::vec3(5.0f, 2.5f, 7.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    RenderScene(shadowMat, lightPosition);
    
    RenderLight(lightPosition);
    
    DepthDebug::Instance()->RenderTexture(depthTextureId);
    
    glutSwapBuffers();
}

void RenderLight(glm::vec3 normPosition) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(projection));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(modelView));
    
    normPosition *= -3.0f;
    glTranslatef(normPosition.x, normPosition.y, normPosition.z);
    glScalef(0.25f, 0.25f, 0.25f);
    
    glColor3f(1.0f, 1.0f, 1.0f);
    sphere->RenderFixedFunction();
}

void updateTimer(int windowId) {
    double time = GetMilliseconds();
    float deltaTime = float(time - lastTime) * 0.001f;
    lastTime = time;
    
    cameraAngle.x += 180.0f * deltaTime;
    glutTimerFunc(UPDATE_TIME, updateTimer, windowId);
    glutPostRedisplay();
}

void reshape(int width, int height) {
    g_nWidth = width;
    g_nHeight = height;
    glViewport(0, 0, width, height);
    projection = glm::perspective(60.0f, float(width) / float(height), 0.01f, 1000.0f);
}

double GetMilliseconds() {
#if __linux | __APPLE__
    static timeval s_tTimeVal;
    gettimeofday(&s_tTimeVal, NULL);
    double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
    time += s_tTimeVal.tv_usec / 1000.0; // us to ms
    return time;
#elif _WIN32
        static LARGE_INTEGER s_frequency;
    static BOOL s_use_qpc = QueryPerformanceFrequency(&s_frequency);
    if (s_use_qpc) {
        LARGE_INTEGER now;
        QueryPerformanceCounter(&now);
        return (double)((1000LL * now.QuadPart) / s_frequency.QuadPart);
    } else {
        return GetTickCount();
    }
#else
        #error implement get millis
#endif
}