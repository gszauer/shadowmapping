#include "DepthDebug.h"
#include <string>
#include <iostream>
#include <assert.h>
#include <vector>

#if __APPLE__
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#elif __linux
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#elif _WIN32
    #include <Windows.h>
    #include <gl/GL.h>
    #include <gl/GLU.h>
    #include "GL/openglut.h"
#else
    #error Include OpenGL Headers
#endif
#if __linux
    #include <glm/glm.hpp>
    #include <glm/ext.hpp>
#else
    #include "glm/glm.hpp"
    #include "glm/ext.hpp"
#endif

using std::cout;
using std::string;
using std::vector;

DepthDebug* DepthDebug::Instance() {
    static DepthDebug instance;
    return &instance;
}

DepthDebug::DepthDebug() {
    string vert_shader;
    string frag_shader;
    
    vert_shader = "#version 120\n\n";
    vert_shader += "attribute vec3 inPosition;\n";
    vert_shader += "attribute vec2 inTexcoord;\n\n";
    vert_shader += "varying vec2 outTexCoord;\n\n";
    vert_shader += "void main() {\n";
    vert_shader += "\toutTexCoord = inTexcoord;\n";
    vert_shader += "\tgl_Position = vec4(inPosition, 1.0);\n";
    vert_shader += "}";
    
    frag_shader = "#version 120\n\n";
    frag_shader += "uniform sampler2D inTexture;\n";
    frag_shader += "varying vec2 outTexCoord;\n\n";
    frag_shader += "void main() {\n";
    frag_shader += "\tgl_FragColor = texture2D(inTexture, outTexCoord.xy);\n";
    frag_shader += "}";
    
    GLint result;
    
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    if (vertexShader == 0) {
        std::cout << "Error creating vertex shader!\n";
    }
    assert(vertexShader);
    
    char* v_shader = (char*)vert_shader.c_str();
    glShaderSource(vertexShader, 1, (const GLchar**)&v_shader, 0);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Error compiling vertex shader!\n";
        glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetShaderInfoLog(vertexShader, result, &result, log);
            std::cout << "Vertex log: " << log << "!\n";
            delete[] log;
        }
        result = GL_FALSE;
    }
    assert(result != GL_FALSE);
    
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    if (fragmentShader == 0) {
        std::cout << "Error creating fragment shader!\n";
    }
    assert(fragmentShader);
    
    char* f_shader = (char*)frag_shader.c_str();
    glShaderSource(fragmentShader, 1, (const GLchar**)&f_shader, 0);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Error compiling fragment shader!\n";
        glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetShaderInfoLog(fragmentShader, result, &result, log);
            std::cout << "Fragment log:" << log << "\n";
            delete[] log;
        }
        result = GL_FALSE;
    }
    assert(result != GL_FALSE);
    
    m_nShaderHandle = glCreateProgram();
    if (m_nShaderHandle == 0)
        std::cout << "Error creating shader program!\n";
    assert(m_nShaderHandle != 0);
    glAttachShader(m_nShaderHandle, vertexShader);
    glAttachShader(m_nShaderHandle, fragmentShader);
    glLinkProgram(m_nShaderHandle);
    glGetProgramiv(m_nShaderHandle, GL_LINK_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Error linking shader program!\n";
        glGetProgramiv(m_nShaderHandle, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetProgramInfoLog(m_nShaderHandle, result, &result, log);
            std::cout << "Program log: " << log << "\n";
            delete[] log;
        }
        result = GL_FALSE;
    }
    assert(result != GL_FALSE);
    
    m_nTextureUniform = glGetUniformLocation(m_nShaderHandle, "inTexture");
    m_nPositionAttribute = glGetAttribLocation(m_nShaderHandle, "inPosition");
    m_nTexCordAttribute = glGetAttribLocation(m_nShaderHandle, "inTexcoord");
    
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    std::vector<unsigned int> indices;
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(2);
    indices.push_back(3);
    indices.push_back(0);
    
    std::vector<float> verts;
    /* pos 1 */verts.push_back(0.5); verts.push_back(0.5); verts.push_back(0.0);
    /* uv 1 */verts.push_back(0.0); verts.push_back(0.0);
    
    /* pos 2 */verts.push_back(1.0); verts.push_back(0.5); verts.push_back(0.0);
    /* uv 2 */verts.push_back(1.0); verts.push_back(0.0);
    
    /* pos 3 */verts.push_back(1.0); verts.push_back(1.0); verts.push_back(0.0);
    /* uv 3 */verts.push_back(1.0); verts.push_back(1.0);
    
    /* pos 4 */verts.push_back(0.5); verts.push_back(1.0); verts.push_back(0.0);
    /* uv 4 */verts.push_back(0.0); verts.push_back(1.0);
    
    glGenBuffers(1, &m_nIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_nIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &m_nVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_nVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, (sizeof(float) * 5) * verts.size(), &verts[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

DepthDebug::~DepthDebug() {
    glDeleteProgram(m_nShaderHandle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &m_nIndexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &m_nVertexBuffer);
}

void DepthDebug::RenderTexture(unsigned int texture) {
    assert(texture != 0);
    
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glUseProgram(m_nShaderHandle);
    
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(m_nTextureUniform, 0);
    glBindTexture(GL_TEXTURE_2D, texture);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_nVertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_nIndexBuffer);
    
    glEnableVertexAttribArray(m_nPositionAttribute);
    glEnableVertexAttribArray(m_nTexCordAttribute);
    
    glVertexAttribPointer(m_nPositionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
    glVertexAttribPointer(m_nTexCordAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(3 * sizeof(GLfloat)));
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    
    glDisableVertexAttribArray(m_nPositionAttribute);
    glDisableVertexAttribArray(m_nTexCordAttribute);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
}