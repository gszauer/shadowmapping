#include "Shader.h"
#if __APPLE__
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#elif __linux
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#elif _WIN32
    #include <Windows.h>
    #include <gl/GL.h>
    #include <gl/GLU.h>
    #include "GL/openglut.h"
#else
    #error Include OpenGL Headers
#endif
#include <istream>
#include <iostream>
#include <fstream>
#include <algorithm>

void Shader::PopulateUniforms() {
    glUseProgram(m_nShaderId);
    GLint   uniforms, length;
    glGetProgramiv(m_nShaderId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &length);
    glGetProgramiv(m_nShaderId, GL_ACTIVE_UNIFORMS, &uniforms);
    GLsizei written; GLint size; GLenum type; GLint location;
    char* name = new char[length + 1];
    
    for (int i = 0; i < uniforms; ++i) {
        memset(name, 0, length + 1);
        glGetActiveUniform(m_nShaderId, i, length, &written, &size, &type, name);
        location = glGetUniformLocation(m_nShaderId, name);
        if (location == -1) {
            std::cout << "Uniform \"" << name << "\" does not exist!\n";
            continue;
        }
        std::string strName = name;
        
        m_vUniformLocations.push_back(location);
        m_vUniformNames.push_back(strName);
        switch (type) {
            case GL_FLOAT:
                m_vUniformTypes.push_back(kUniformFloat);
                break;
            case GL_FLOAT_VEC4:
                m_vUniformTypes.push_back(kUniformVector);
                break;
            case GL_FLOAT_MAT4:
                m_vUniformTypes.push_back(kUniformMatrix);
                break;
            case GL_INT:
                m_vUniformTypes.push_back(kUniformInt);
                break;
            case GL_SAMPLER_2D:
                m_vUniformTypes.push_back(kUniformSampler);
            default:
                m_vUniformTypes.push_back(kUniformUnsupported);
                break;
        }
        m_mapUniformLocations[strName] = location;
    }
    
    delete[] name;
}

std::string Shader::ReadFile(const char* path) {
    std::ifstream file(path, std::ifstream::in);
    if (!file) {
        std::cout << "Couldn't load file: " << path << '\n';
        m_bError = true;
        return "";
    }
    std::string result;
    file.seekg(0, std::ios::end);
    result.reserve(file.tellg());
    file.seekg(0, std::ios::beg);
    result.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    file.close();
    
    return result;
}

unsigned int Shader::CompileShader(const char* shader, unsigned int type) {
    GLuint result = glCreateShader(type);
    GLint status;
    if (result == 0) {
        std::cout << "Could not create shader!\n";
        m_bError = true;
        return 0;
    }
    int shader_length = int(strlen(shader));
    glShaderSource(result, 1, (const GLchar**)&shader, &shader_length);
    glCompileShader(result);
    glGetShaderiv(result, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        std::cout << "Could not compile shader!\n";
        glGetShaderiv(result, GL_INFO_LOG_LENGTH, &status);
        if (status > 0) {
            char* log = new char[status];
            glGetShaderInfoLog(result, status, &status, log);
            std::cout << "\t: " << log << "!\n";
            delete[] log;
        }
        m_bError = true;
        return 0;
    }
    
    return result;
}

unsigned int Shader::MakeProgram(unsigned int vertex, unsigned int fragment) {
    GLuint shaderId = glCreateProgram();
    GLint result;
    if (shaderId == 0) {
        std::cout << "Could not create shader program!\n";
        m_bError = true;
        return 0;
    }
    glAttachShader(shaderId, vertex);
    glAttachShader(shaderId, fragment);
    glLinkProgram(shaderId);
    glGetProgramiv(shaderId, GL_LINK_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Could not link shader program!\n";
        glGetProgramiv(shaderId, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetProgramInfoLog(shaderId, result, &result, log);
            std::cout << "\t: " << log << "\n";
            delete[] log;
        }
        m_bError = true;
        return 0;
    }
    
    return shaderId;
}

Shader::Shader(const char* shader) {
    m_bError = false;
    
    char vertexSource[256];
    sprintf(vertexSource, "%s.vs", shader);
    char fragmentSource[256];
    sprintf(fragmentSource, "%s.fs", shader);
    
    std::string vertexShaderStr = ReadFile(vertexSource);
    std::string fragmentShaderStr = ReadFile(fragmentSource);
    if (m_bError) return;
    
    GLuint vertexShader = CompileShader(vertexShaderStr.c_str(), GL_VERTEX_SHADER);
    GLuint fragmentShader = CompileShader(fragmentShaderStr.c_str(), GL_FRAGMENT_SHADER);
    if (m_bError) return;
    
    m_nShaderId = MakeProgram(vertexShader, fragmentShader);
    if (m_bError) return;
    
    PopulateUniforms();
    
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    glUseProgram(0);
}

bool Shader::GetError() {
    return m_bError;
}

Shader::~Shader() {
    glDeleteProgram(m_nShaderId);
}

void Shader::Bind() {
    glUseProgram(m_nShaderId);
}

void Shader::Unbind() {
    glUseProgram(0);
}

unsigned int Shader::GetHandle() {
    return m_nShaderId;
}

int Shader::GetUniformCount() {
    return int(m_vUniformNames.size());
}

std::string Shader::GetUniformName(int index) {
    return m_vUniformNames[index];
}

int Shader::GetUniformIndex(const std::string& name) {
    std::vector<std::string>::iterator it =  std::find(m_vUniformNames.begin(), m_vUniformNames.end(), name);
    size_t index = it - m_vUniformNames.begin();
    return int(index);
}

int Shader::GetNumUniforms() {
    return int(m_vUniformTypes.size());
}

unsigned int Shader::GetUniformType(int index) {
    return m_vUniformTypes[index];
}

bool Shader::ContainsUniform(const std::string& name) {
    std::vector<std::string>::iterator it =  std::find(m_vUniformNames.begin(), m_vUniformNames.end(), name);
    return it != m_vUniformNames.end();
}

unsigned int Shader::GetUniformLocation(int index) {
    return m_vUniformLocations[index];
}

unsigned int Shader::GetUniformLocation(const std::string& name) {
    return m_mapUniformLocations[name];
}

void Shader::SetInt(int uniform, int value) {
    GLint location = m_vUniformLocations[uniform];
    glUniform1i(location, (GLint)value);
}

void Shader::SetFloat(int uniform, float value) {
    GLint location = m_vUniformLocations[uniform];
    glUniform1f(location, value);
}

void Shader::SetVector(int uniform, const glm::vec4& value) {
    GLint location = m_vUniformLocations[uniform];
    glUniform4f(location, value.x, value.y, value.z, value.w);
}

void Shader::SetMatrix(int uniform, const glm::mat4& value) {
    GLint location = m_vUniformLocations[uniform];
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
}

void Shader::SetBool(int uniform, bool value) {
    GLint location = m_vUniformLocations[uniform];
    glUniform1i(location, value? 1 : 0);
}

void Shader::SetInt(const std::string& uniform, int value) {
    GLint location = m_mapUniformLocations[uniform];
    glUniform1i(location, (GLint)value);
}

void Shader::SetFloat(const std::string& uniform, float value) {
    GLint location = m_mapUniformLocations[uniform];
    glUniform1f(location, value);
}

void Shader::SetVector(const std::string& uniform, const glm::vec4& value) {
    GLint location = m_mapUniformLocations[uniform];
    glUniform4f(location, value.x, value.y, value.z, value.w);
}
void Shader::SetMatrix(const std::string& uniform, const glm::mat4& value) {
    GLint location = m_mapUniformLocations[uniform];
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
}

void Shader::SetBool(const std::string& uniform, bool value) {
    GLint location = m_mapUniformLocations[uniform];
    glUniform1i(location, value? 1 : 0);
}

int Shader::GetAttribLocation(const std::string& attribName) {
    return glGetAttribLocation(m_nShaderId, attribName.c_str());
}