#ifndef _H_DEPTHDEBUG_
#define _H_DEPTHDEBUG_

class DepthDebug {
protected:
    unsigned int        m_nShaderHandle;
    unsigned int        m_nTextureUniform;
    unsigned int        m_nPositionAttribute;
    unsigned int        m_nTexCordAttribute;
    unsigned int        m_nIndexBuffer;
    unsigned int        m_nVertexBuffer;
protected:
    DepthDebug();
    DepthDebug(const DepthDebug&);
    DepthDebug& operator=(const DepthDebug&);
public:
    ~DepthDebug();
    static DepthDebug* Instance();
    
    void RenderTexture(unsigned int texture);
};

#endif